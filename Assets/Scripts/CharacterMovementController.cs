﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterMovementController : MonoBehaviour
{
    public enum Facing
    {
        Left,
        Right,
    }

    [SerializeField] PlayerData playerData;
    [SerializeField] BoxCollider2D groundCheck;
    [SerializeField] Animator animator;
    [SerializeField] Transform damageArea;
    [SerializeField] Rigidbody2D myRigidbody;

    [SerializeField] Collider2D myCollider;

    InputManager inputManager;
    Facing currentFacing = Facing.Right;

    bool isJumping = false;
    bool hasAttacked = false;

    float attackCd = 0;

    private void Awake()
    {
        playerData = Instantiate(playerData);
        inputManager = InputManager.instance;
        UIController.instance.UpdateMoneyText(playerData.money);
    }

    // Update is called once per frame
    void Update()
    {
        if (playerData.isAlive)
        {
            HandleMovement();
            HandleAttack();
        }
    }

    private void OnDrawGizmos()
    {
        if (!damageArea)
            return;

        Gizmos.DrawWireSphere(damageArea.position, playerData.attackRange);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Ground"))
        {
            isJumping = false;
            animator.SetBool("Jump", isJumping);
        }
    }

    void HandleMovement()
    {
        if(inputManager.PlayerMovingLeftRight() != 0)
        {
            animator.SetFloat("PlayerSpeed", Mathf.Abs(inputManager.PlayerMovingLeftRight()));
            Debug.LogError("Added force " + inputManager.PlayerMovingLeftRight() * playerData.movementSpeed);
            myRigidbody.AddForce(new Vector2(inputManager.PlayerMovingLeftRight() * playerData.movementSpeed, 0), ForceMode2D.Impulse);
            HandleFacingAndFlipIfNeeded();
        }
        else
        {
            animator.SetFloat("PlayerSpeed",0);
        }

        if (inputManager.PlayerJump() && !isJumping)
        {
            isJumping = true;
            myRigidbody.AddForce(new Vector2(0, playerData.jumpForce), ForceMode2D.Impulse);
            animator.SetBool("Jump", isJumping);
        }

    }

    private void HandleFacingAndFlipIfNeeded()
    {
        if (!hasAttacked)
        {
            if (inputManager.PlayerMovingLeftRight() > 0 && currentFacing != Facing.Right)
            {
                currentFacing = Facing.Right;
                transform.localEulerAngles = new Vector3(0, 0, 0);
            }
            else if (inputManager.PlayerMovingLeftRight() < 0 && currentFacing != Facing.Left)
            {
                transform.localEulerAngles = new Vector3(0, 180, 0);
                currentFacing = Facing.Left;
            }
        }
    }

    private void HandleAttack()
    {
        if (inputManager.PlayerAttack() && !hasAttacked)
        {
            hasAttacked = true;
            animator.SetTrigger("Attack");
            List<Collider2D> allHitObjects = Physics2D.OverlapCircleAll(damageArea.position, playerData.attackRange, 1 << Globals.layer_EnemyLayer).AsEnumerable().ToList();
            foreach(Collider2D hitObject in allHitObjects)
            {
                hitObject.GetComponent<EnemyCharacter>().DamageUnit(playerData.playerDamage);
            }
        }

        if (hasAttacked)
        {
            attackCd += Time.deltaTime;
            if(attackCd >= playerData.attackCd)
            {
                hasAttacked = false;
                attackCd = 0;
            }
        }
    }

    public void DealDamage(int damage, Vector3 enemyPosition)
    {
        if (playerData.isAlive)
        {
            myRigidbody.AddForce((transform.position - enemyPosition) * playerData.hurtForce, ForceMode2D.Impulse);
            playerData.playerCurrentHealth -= damage;
            UIController.instance.KillOneHearth();
            if (playerData.playerCurrentHealth <= 0)
                Die();
        }
    }

    void Die()
    {
        myCollider.enabled = false;
        myRigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        playerData.isAlive = false;
        animator.SetTrigger("Die");
    }

    internal void CollectMoney()
    {
        playerData.money += 1;
        UIController.instance.UpdateMoneyText(playerData.money);
    }
}
