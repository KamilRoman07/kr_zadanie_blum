﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] Transform targetToFollow;
    [SerializeField] Vector3 cameraOffset;
    [SerializeField] [Range(0, 10)] int smoothFactor;

    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, targetToFollow.position + cameraOffset, Time.fixedDeltaTime * smoothFactor);
    }
}
