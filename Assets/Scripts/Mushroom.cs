﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : EnemyCharacter
{
    MushroomData data;

    public override void Awake()
    {
        base.Awake();
        data = enemyData.Instantiate() as MushroomData;
    }
}
