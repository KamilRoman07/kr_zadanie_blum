﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WanzyeeStudio;

public class InputManager : BaseSingleton<InputManager>
{
    [SerializeField] KeyCode runLeft = KeyCode.A;
    [SerializeField] KeyCode runRight = KeyCode.D;
    [SerializeField] KeyCode jump = KeyCode.W;
    [SerializeField] KeyCode attack = KeyCode.K;

   public int PlayerMovingLeftRight()
    {
        if (Input.GetKey(runLeft))
            return -1;
        else if (Input.GetKey(runRight))
            return 1;
        else
            return 0;
    }

    public bool PlayerJump()
    {
        return Input.GetKeyDown(jump);
    }

    public bool PlayerAttack()
    {
        return Input.GetKeyDown(attack);
    }
}
