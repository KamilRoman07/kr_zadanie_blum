﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CharacterMovementController;

public class EnemyCharacter : MonoBehaviour
{
    [SerializeField] protected List<Transform> patrolPoints;
    [SerializeField] protected Rigidbody2D myRigidbody;
    [SerializeField] protected Animator animator;
    [SerializeField] protected Collider2D myCollider;

    RigidbodyConstraints2D baseConstraints = RigidbodyConstraints2D.FreezeRotation;

    public EnemyData enemyData;
    Transform currentPatrolPoint;

    protected bool isInterupted = false;

    protected Facing currentFacing = Facing.Right;

    public virtual void Awake()
    {
        if (patrolPoints.Count > 0)
            currentPatrolPoint = patrolPoints[0];

        enemyData = enemyData.Instantiate();
    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer.Equals(Globals.layer_MainCharacter) && !isInterupted)
        {
            collision.gameObject.GetComponent<CharacterMovementController>().DealDamage(enemyData.damage, transform.position);
        }
           
    }

    public virtual void Update()
    {
        if (!isInterupted)
            Patrol();

        HandleFacingAndFlipIfNeeded();
    }

    private void HandleFacingAndFlipIfNeeded()
    {
        if (!isInterupted && currentPatrolPoint)
        {
            if (currentPatrolPoint.position.x > transform.position.x && currentFacing != Facing.Right)
            {
                currentFacing = Facing.Right;
                transform.localEulerAngles = new Vector3(0, 0, 0);
            }
            else if (currentPatrolPoint.position.x < transform.position.x && currentFacing != Facing.Left)
            {
                transform.localEulerAngles = new Vector3(0, 180, 0);
                currentFacing = Facing.Left;
            }
        }
    }

    void Patrol()
    {
        if (currentPatrolPoint && Vector3.Distance(transform.position, currentPatrolPoint.position) > 1f)
        {
            animator.SetFloat("Speed", 1);
            myRigidbody.AddForce((currentPatrolPoint.position - transform.position).normalized * enemyData.movementSpeed, ForceMode2D.Impulse);
        }
        else if (patrolPoints.Count > 1)
        {
            foreach (Transform patrolPoint in patrolPoints)
                if (patrolPoint != currentPatrolPoint)
                {
                    currentPatrolPoint = patrolPoint;
                    break;
                }
        }
        else
            animator.SetFloat("Speed", 0);
    }

    public virtual void DamageUnit(int playerDamage)
    {
        Debug.LogError("Got damaged "+gameObject.name);
        animator.SetFloat("Speed", 0);
        animator.SetTrigger("Attacked");
        isInterupted = true;
        myRigidbody.constraints = baseConstraints | RigidbodyConstraints2D.FreezePositionX;
        StartCoroutine(WaitForHitAnimation(animator.GetCurrentAnimatorStateInfo(0).length, playerDamage));
    }

    protected virtual IEnumerator WaitForHitAnimation(float time, int playerDamage)
    {
        enemyData.hp -= playerDamage;
        if (enemyData.hp <= 0)
            Die();
        else
        {
            myRigidbody.constraints = baseConstraints;
            yield return new WaitForSeconds(time);
            isInterupted = false;
        }
    }

    public virtual void Die()
    {
        isInterupted = true;
        myRigidbody.gravityScale = 0;
        myCollider.enabled = false;
        animator.SetTrigger("Die");
        StartCoroutine(DelayDeath(animator.GetCurrentAnimatorStateInfo(0).length));
    }

    private IEnumerator DelayDeath(float length)
    {
        yield return new WaitForSeconds(length);
        Destroy(gameObject);
    }
}
