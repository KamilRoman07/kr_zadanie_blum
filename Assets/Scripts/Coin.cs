﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] Animator animator;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer.Equals(Globals.layer_MainCharacter))
        {
            collision.gameObject.GetComponent<CharacterMovementController>().CollectMoney();
            Destroy(gameObject);
        }
    }
}
