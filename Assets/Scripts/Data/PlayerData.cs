﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "Data/Player/PlayerData", order = 1)]
public class PlayerData : ScriptableObject
{
    public bool isAlive = true;
    public int playerMaxHealth = 3;
    public int playerCurrentHealth = 3;
    public int playerDamage = 1;
    public float attackCd = 1f;
    public float movementSpeed;
    public float jumpForce;
    public float attackRange = 0.5f;
    public float hurtForce = 15;
    public int money = 0;
}
