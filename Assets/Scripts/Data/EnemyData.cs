﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyData : ScriptableObject
{
    [HideInInspector]
    public Guid enemyID = System.Guid.NewGuid();
    public int hp;
    public int damage;
    public float movementSpeed = 0.1f;

    public EnemyData Instantiate()
    {
        EnemyData clone = Instantiate(this);
        enemyID = System.Guid.NewGuid();
        return clone;
    }
}
