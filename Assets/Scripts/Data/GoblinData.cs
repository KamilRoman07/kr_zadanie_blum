﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GoblinData", menuName = "Data/Enemy/Goblin", order = 1)]
public class GoblinData : EnemyData
{
    public float attackRange = 0.3f;
    public float playerDetectionRange = 1f;
    public float attackCooldown = 3f;
    public float attackDelay = 0.33f;
}
