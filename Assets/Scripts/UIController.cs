﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using WanzyeeStudio;

public class UIController : BaseSingleton<UIController>
{
    [SerializeField] Text moneyText;
    [SerializeField] Transform hearthBox;
    List<Hearth> allHearths;

    private void Awake()
    {
        allHearths = gameObject.GetComponentsInChildren<Hearth>().AsEnumerable().ToList();
    }

    public void UpdateMoneyText(int moneyValue)
    {
        moneyText.text = "X" + moneyValue;
    }

    public void KillOneHearth()
    {
        for (int i = allHearths.Count - 1; i >= 0; i--)
        {
            if (allHearths[i].isAlive)
            {
                allHearths[i].Die();
                break;
            }
        }
    }
}
