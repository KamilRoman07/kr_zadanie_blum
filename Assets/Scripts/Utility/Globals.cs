﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Globals
{
    public const int layer_EnemyLayer = 10;
    public const int layer_MainCharacter = 9;
}
