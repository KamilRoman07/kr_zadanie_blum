﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CharacterMovementController;

public class Goblin : EnemyCharacter
{
    [SerializeField] Transform damageArea;
    bool onAttackCooldown = false;
    bool interuptedForAttack = false;
    GoblinData goblinData;
    float attackCooldownTimer = 0;

    // Start is called before the first frame update
    public override void Awake()
    {
        base.Awake();
        goblinData = Instantiate(enemyData as GoblinData);
    }

    // Update is called once per frame
    public override void Update()
    {
        if (!interuptedForAttack)
        {
            base.Update();
            myRigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
        Collider2D[] playerHit = Physics2D.OverlapCircleAll(transform.position, goblinData.playerDetectionRange, 1 << Globals.layer_MainCharacter);
        if (playerHit.Length > 0 && !isInterupted)
        {
            Attack(playerHit[0]);
        }

        if (onAttackCooldown)
        {
            attackCooldownTimer += Time.deltaTime;
            if (attackCooldownTimer >= goblinData.attackCooldown)
            {
                onAttackCooldown = interuptedForAttack = false;
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (!damageArea)
            return;

        Gizmos.DrawWireSphere(damageArea.position, (enemyData as GoblinData).attackRange);
        Gizmos.DrawWireSphere(transform.position, (enemyData as GoblinData).playerDetectionRange);
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
    }

    void Attack(Collider2D playerHit)
    {
        if (!onAttackCooldown)
        {
            FlipForAttack(playerHit);
            onAttackCooldown = interuptedForAttack = true;
            myRigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
            attackCooldownTimer = 0;
            StartCoroutine(CommenceAttackWithDelay(goblinData.attackDelay));
        }
    }

    void FlipForAttack(Collider2D playerHit)
    {
        if (playerHit.transform.position.x > transform.position.x && currentFacing != Facing.Right)
        {
            currentFacing = Facing.Right;
            transform.localEulerAngles = new Vector3(0, 0, 0);
        }
        else if (playerHit.transform.position.x < transform.position.x && currentFacing != Facing.Left)
        {
            transform.localEulerAngles = new Vector3(0, 180, 0);
            currentFacing = Facing.Left;
        }
    }

    IEnumerator CommenceAttackWithDelay(float attackDelay)
    {
        yield return new WaitForSeconds(attackDelay);

        animator.SetTrigger("Attack");
        Collider2D[] allHitObjects = Physics2D.OverlapCircleAll(damageArea.position, goblinData.attackRange, 1 << Globals.layer_MainCharacter);
        allHitObjects[0].GetComponent<CharacterMovementController>().DealDamage(goblinData.damage, damageArea.position);
    }
}
