﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hearth : MonoBehaviour
{
    [SerializeField] Sprite fullHp;
    [SerializeField] Sprite emptyHp;
    [SerializeField] Animator animator;
    [SerializeField] Image myImage;
    public bool isAlive = true;

    private void Awake()
    {
        myImage.sprite = fullHp;
    }

    public void Die()
    {
        isAlive = false;
        animator.Play("Hearth_Die", -1, 0f);
    }
}
